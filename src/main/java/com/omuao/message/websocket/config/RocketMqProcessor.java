package com.omuao.message.websocket.config;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

/**
 * RocketMQ 消息处理器
 *
 * @author omuao
 */
@Configuration
@Component
public interface RocketMqProcessor {

    /**
     * 延迟输入通道
     */
    String NOTIFY_INPUT = "system_topic_queue_notify_input";

    /**
     * 延迟输出通道
     */
    String NOTIFY_OUTPUT = "system_topic_queue_notify_output";

    /**
     * 延迟输入通道
     *
     * @return
     */
    @Input(RocketMqProcessor.NOTIFY_INPUT)
    SubscribableChannel input();

    /**
     * 延迟输出通道
     *
     * @return
     */
    @Output(RocketMqProcessor.NOTIFY_OUTPUT)
    MessageChannel output();


}
