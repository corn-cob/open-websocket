package com.omuao.message.websocket.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 延时队列属性
 *
 * @author omuao
 */
@Configuration
@ConfigurationProperties(prefix = "queue")
@PropertySource(value = "cache-queue.properties")
public class CacheQueueProperties {

    /**
     * 数据库路径
     */
    private String dbPath = "cache-queue.db";

    /**
     * 数据库路径
     */
    private String sessionDbPath = "session-queue.db";


    public String getDbPath() {
        return dbPath;
    }

    public void setDbPath(String dbPath) {
        this.dbPath = dbPath;
    }

    public String getSessionDbPath() {
        return sessionDbPath;
    }

    public void setSessionDbPath(String sessionDbPath) {
        this.sessionDbPath = sessionDbPath;
    }
}
