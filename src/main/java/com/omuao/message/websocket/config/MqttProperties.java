package com.omuao.message.websocket.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * RocketMQ属性
 *
 * @author omuao
 */
@Component
@ConfigurationProperties(prefix = "mqtt")
@PropertySource(value = "mqtt.properties")
public class MqttProperties {

    private String superClientId;

    private String brokerAddress;

    private String superUsername;

    private String superPassword;

    private String topicPrefix;

    public String getSuperClientId() {
        return superClientId;
    }

    public void setSuperClientId(String superClientId) {
        this.superClientId = superClientId;
    }

    public String getBrokerAddress() {
        return brokerAddress;
    }

    public void setBrokerAddress(String brokerAddress) {
        this.brokerAddress = brokerAddress;
    }

    public String getSuperUsername() {
        return superUsername;
    }

    public void setSuperUsername(String superUsername) {
        this.superUsername = superUsername;
    }

    public String getSuperPassword() {
        return superPassword;
    }

    public void setSuperPassword(String superPassword) {
        this.superPassword = superPassword;
    }

    public String getTopicPrefix() {
        return topicPrefix;
    }

    public void setTopicPrefix(String topicPrefix) {
        this.topicPrefix = topicPrefix;
    }
}
