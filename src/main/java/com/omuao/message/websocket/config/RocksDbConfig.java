package com.omuao.message.websocket.config;

import org.rocksdb.Options;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksDBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

/**
 * LEVEL db 配置
 *
 * @author omuao
 */
@Configuration
public class RocksDbConfig {

    public static Logger logger = LoggerFactory.getLogger(RocksDbConfig.class);

    public static final String ROCK_DB_WRITE = "ROCK_DB_WRITE";

    public static final String ROCKS_ITERATOR_WRITE = "ROCK_DB_WRITE";

    public static final String ROCK_DB_SESSION = "ROCK_DB_SESSION";

    public static final String ROCKS_ITERATOR_SESSION = "ROCK_DB_WRITE";

    @Autowired
    CacheQueueProperties cacheQueueProperties;

    static {
        RocksDB.loadLibrary();
    }

    /**
     * 开启一个连接
     *
     * @return
     * @throws IOException 输出异常
     */
    @Bean(ROCK_DB_WRITE)
    public RocksDB rocksDB() throws IOException {
        Options options = new Options();
        options.setCreateIfMissing(true);
        File file = new File(cacheQueueProperties.getDbPath());
        RocksDB db = null;
        try {
            db = RocksDB.open(options, file.getPath());
        } catch (RocksDBException e) {
            logger.error(e.getMessage(), e);
        }
        return db;
    }

    /**
     * 开启一个连接
     *
     * @return
     * @throws IOException 输出异常
     */
    @Bean(ROCK_DB_SESSION)
    public RocksDB rocksSessionDB() throws IOException {
        Options options = new Options();
        options.setCreateIfMissing(true);
        File file = new File(cacheQueueProperties.getSessionDbPath());
        RocksDB db = null;
        try {
            db = RocksDB.open(options, file.getPath());
        } catch (RocksDBException e) {
            logger.error(e.getMessage(), e);
        }
        return db;
    }

}
