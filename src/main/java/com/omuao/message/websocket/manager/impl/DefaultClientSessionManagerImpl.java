package com.omuao.message.websocket.manager.impl;

import com.omuao.message.websocket.facade.ClientSessionManager;

import java.util.concurrent.ConcurrentSkipListSet;

/**
 * 默认客户端Session Manager
 *
 * @author
 */
//@Repository
public class DefaultClientSessionManagerImpl implements ClientSessionManager {

    /**
     * WebSocket消费者 (Redis 在线客户)
     */
    private static ConcurrentSkipListSet<String> MQ_CONSUMER_CONCURRENT_SET = new ConcurrentSkipListSet<>();

    @Override
    public void put(String clientId) {
        MQ_CONSUMER_CONCURRENT_SET.add(clientId);
    }

    @Override
    public void remove(String clientId) {
        if (MQ_CONSUMER_CONCURRENT_SET.contains(clientId)) {
            MQ_CONSUMER_CONCURRENT_SET.remove(clientId);
        }
    }

    @Override
    public boolean contains(String clientId) {
        return MQ_CONSUMER_CONCURRENT_SET.contains(clientId);
    }
}
