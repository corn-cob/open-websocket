package com.omuao.message.websocket.manager.impl;

import com.omuao.message.websocket.facade.ClientSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.concurrent.TimeUnit;

/**
 * redis 客户端会话管理器
 *
 * @author omuao
 */
@Repository
@Primary
public class RedisClientSessionManagerImpl implements ClientSessionManager {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Override
    public void put(String clientId) {
        stringRedisTemplate.opsForValue().set("com:omuao:session:" + clientId, "1", TimeUnit.MINUTES.toMillis(10));
    }

    @Override
    public void remove(String clientId) {
        stringRedisTemplate.delete("com:omuao:session:" + clientId);
    }

    @Override
    public boolean contains(String clientId) {
        String string = stringRedisTemplate.opsForValue().get("com:omuao:session:" + clientId);
        if (string != null) {
            return true;
        }
        return false;
    }
}
