package com.omuao.message.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * webSocket 服务
 *
 * @author omauo
 */
@SpringBootApplication
@EnableScheduling
public class WebSocketServer {

    public static void main(String[] args) {
        SpringApplication.run(WebSocketServer.class, args);
    }

}
