package com.omuao.message.websocket.facade;

import com.omuao.message.websocket.error.ServiceException;

import java.util.Map;

/**
 * MQTT websocket 业务
 *
 * @author omuao
 */
public interface MqttWebSocketService {

    /**
     * 处理事件
     *
     * @param map
     * @throws ServiceException 逻辑业务异常
     */
    void processHookEvent(Map<String, Object> map) throws ServiceException;

    /**
     * 认证操作
     *
     * @param username 用户名
     * @param password 密码
     * @param clientId 客户端ID
     * @return
     */
    boolean auth(String username, String password, String clientId);

    /**
     * 发送离线消息
     *
     * @throws ServiceException 业务异常
     */
    void sendOfflineMessage() throws ServiceException;

    /**
     * 插入消息
     *
     * @param webSocketMessage 消息
     * @throws ServiceException 业务异常
     */
    void insert(WebSocketMessage webSocketMessage) throws ServiceException;
}
