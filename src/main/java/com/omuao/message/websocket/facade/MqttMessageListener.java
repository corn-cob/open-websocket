package com.omuao.message.websocket.facade;

/**
 * MQTT 消息监听器
 *
 * @author omuao
 */
public interface MqttMessageListener {

    /**
     * 发送消息
     *
     * @param result 结果
     * @return
     */
    boolean sendMessage(WebSocketMessage result);
}
