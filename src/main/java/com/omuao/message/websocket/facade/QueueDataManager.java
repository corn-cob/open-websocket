package com.omuao.message.websocket.facade;

/**
 * 队列数据管理器
 *
 * @author omuao
 */
public interface QueueDataManager {

    /**
     * 根据key 删除队列数据
     *
     * @param queueData 队列数据
     * @return 受影响条数
     */
    int delete(WebSocketMessage queueData);

    /**
     * 根据key 查询队列数据
     *
     * @param queueData 队列数据
     * @return 队列数据
     */
    WebSocketMessage query(WebSocketMessage queueData);

    /**
     * 根据key 插入队列数据
     *
     * @param queueData 队列数据
     * @return 受影响条数
     */
    int insert(WebSocketMessage queueData);

    /**
     * 根据key 更新队列数据
     *
     * @param queueData 队列数据
     * @return 受影响条数
     */
    int update(WebSocketMessage queueData);

    /**
     * 整理
     */
    void compactRange();

    /**
     * 发送离线消息
     */
    void sendOfflineMessage();
}
