package com.omuao.message.websocket.facade;

/**
 * 客户端会话
 *
 * @author omuao
 */
public interface ClientSessionManager {

    /**
     * 放入Session
     *
     * @param clientId 客户端
     */
    void put(String clientId);

    /**
     * 移除Session
     *
     * @param clientId 客户端
     */
    void remove(String clientId);

    /**
     * 是否存在
     *
     * @param clientId 客户端
     * @return true 存在 false 不存在
     */
    boolean contains(String clientId);
}
