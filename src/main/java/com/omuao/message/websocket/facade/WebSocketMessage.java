package com.omuao.message.websocket.facade;

/**
 * 消息
 *
 * @author omuao
 */
public class WebSocketMessage {

    /**
     * 接受人
     */
    private String receiverId;

    /**
     * 接受人ID
     */
    private String originatorId;

    /**
     * 消息ID
     */
    private String key;

    /**
     * 消息
     */
    private String content;

    /**
     * 消息类型
     */
    private int type;

    /**
     * 消息编码
     */
    private String statusCode;

    public WebSocketMessage() {
    }

    public WebSocketMessage(String statusCode, int type) {
        this.statusCode = statusCode;
        this.type = type;
    }

    public WebSocketMessage(WebSocketMessage message) {
        this.statusCode = message.statusCode;
        this.type = message.type;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getOriginatorId() {
        return originatorId;
    }

    public void setOriginatorId(String originatorId) {
        this.originatorId = originatorId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
