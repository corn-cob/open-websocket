package com.omuao.message.websocket.enums;

/**
 * 消息类型
 *
 * @author omuao
 */
public enum MessageType {

    /**
     * 确认消息
     */
    ACK(-3),

    /**
     * 响应消息
     */
    RESPONSE(-2),

    /**
     * 心跳消息
     */
    HEARTBEAT(-1),
    /**
     * 正常消息
     */
    NORMAL(0),
    /**
     * 文件消息
     */
    FILE(1),
    /**
     * 图像消息
     */
    IMAGE(2),
    /**
     * 语音消息
     */
    AUDIO(3),
    /**
     * 视频消息
     */
    VIDEO(4);

    /**
     * 消息类型
     */
    private int typeValue;

    MessageType(int typeValue) {
        this.typeValue = typeValue;
    }

    public static MessageType valueOf(int typeValue) {
        for (MessageType messageType : MessageType.values()) {
            if (messageType.typeValue == typeValue) {
                return messageType;
            }
        }
        return MessageType.NORMAL;
    }

    public int getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(int typeValue) {
        this.typeValue = typeValue;
    }
}
