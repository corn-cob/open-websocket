package com.omuao.message.websocket.enums;

/**
 * MQTT 行为类型
 * web.hook.rule.client.connected.1     = {"action": "on_client_connected"}
 * web.hook.rule.client.disconnected.1  = {"action": "on_client_disconnected"}
 * web.hook.rule.client.subscribe.1     = {"action": "on_client_subscribe"}
 * web.hook.rule.client.unsubscribe.1   = {"action": "on_client_unsubscribe"}
 * web.hook.rule.session.subscribed.1   = {"action": "on_session_subscribed"}
 * web.hook.rule.session.unsubscribed.1 = {"action": "on_session_unsubscribed"}
 * web.hook.rule.message.publish.1      = {"action": "on_message_publish"}
 * web.hook.rule.message.delivered.1    = {"action": "on_message_delivered"}
 * web.hook.rule.message.acked.1        = {"action": "on_message_acked"}
 *
 * @author omuao
 */
public enum MqttActionType {
    CLIENT_CONNECTED,
    CLIENT_DISCONNECTED,
    CLIENT_SUBSCRIBE,
    CLIENT_UNSUBSCRIBE,
    SESSION_SUBSCRIBED,
    SESSION_UNSUBSCRIBED,
    MESSAGE_PUBLISH,
    MESSAGE_DELIVERED,
    MESSAGE_ACKED;
}
